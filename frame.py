import argparse
import sys
from pathlib import Path
from pandas import DataFrame
import pandas as pd
import time


def test_list(args):
    parser = argparse.ArgumentParser(description='Returns list for python')
    parser.add_argument("--input", type=argparse.FileType('r'), required=True)
    # parser.add_argument("--output", type=str, required=True)
    args = parser.parse_args(args)
    data = pd.read_csv(args.input)
    df = DataFrame(data)
    # Path(args.output).parent.mkdir(parents=True, exist_ok=True)
    # with open(args.output, 'w') as output_path:
    #    output_path.write('{}'.format(df))


if __name__ == '__main__':
    print("start time: ", time.strftime('%H.%M.%S', time.localtime()))
    test_list(sys.argv[1:])
    print("end time: ", time.strftime('%H.%M.%S', time.localtime()))