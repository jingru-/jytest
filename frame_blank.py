import argparse
import sys
from pandas import DataFrame
import pandas as pd
import time


def frame_orc(args):
    parser = argparse.ArgumentParser(description='Returns list for python')
    parser.add_argument("--input", type=argparse.FileType('rb'), required=True)
    args = parser.parse_args(args)


if __name__ == '__main__':
    print("start time: ", time.strftime('%H.%M.%S', time.localtime()))
    frame_orc(sys.argv[1:])
    print("end time: ", time.strftime('%H.%M.%S', time.localtime()))
